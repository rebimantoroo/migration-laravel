@extends('adminlte.master')

@section('judulFile')
  Halaman Selamat Datang
@endsection

@section('judul')
  Selamat Datang
@endsection

@section('isi')
    Selamat datang, silahkan memilih halaman yang anda inginkan
    <br>
    <a href="/table">Halaman Table</a>
    <br>
    <a href="/data-tables">Halaman Data-Tables</a>
@endsection
